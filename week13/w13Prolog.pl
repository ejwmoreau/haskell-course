
% Prologs House of Windsor
%
% Male members of House of Windsor
male(andrew).
male(charles).
male(edward).
male(harry).
male(mark).
male(peter).
male(philip).
male(severn).
male(timothy).
male(william).

% Female members of House of Windsor
female(anne).
female(beatrice).
female(diana).
female(elizabeth_II).
female(eugenie).
female(louise).
female(sophie).
female(sarah).
female(zara).

% Children of Elizabeth II & Philip
child_of(charles,elizabeth_II).
child_of(charles,philip).
child_of(anne,elizabeth_II).
child_of(anne,philip).
child_of(andrew,elizabeth_II).
child_of(andrew,philip).
child_of(edward,elizabeth_II).
child_of(edward,philip).

% Children of Diana & Charles
child_of(william,charles).
child_of(william,diana).
child_of(harry,charles).
child_of(harry,diana).

% Children of Anne & Mark
child_of(zara,anne).
child_of(zara,mark).
child_of(peter,anne).
child_of(peter,mark).

% Children of Sarah & Andrew
child_of(beatrice,sarah).
child_of(beatrice,andrew).
child_of(eugenie,sarah).
child_of(eugenie,andrew).

% Children of Sophie & Edward
child_of(louise,sophie).
child_of(louise,edward).
child_of(severn,sophie).
child_of(severn,edward).

% Married
married(philip,elizabeth_II).
married(charles,diana).
married(charles,camilla).
married(mark,anne).
married(timothy,anne).
married(andrew,sarah).
married(edward,sophie).

%%%%% Tutorial Exercises %%%%%

%%% Exercise 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Run Prolog using `swipl`
% Load above facts using `?- [w13Prolog].`


%%% Exercise 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define rule for `mother_of(Person, Child)`
% that holds if Person is the mother of Child

% Rule
mother_of(Person, Child) :- child_of(Child, Person), female(Person).

% Tests
% mother_of(diana, william). => true
% mother_of(diana, charles). => false
% mother_of(A, B).


%%% Exercise 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define rule `mother(Person)`
% that holds if Person is a mother

% Rule
mother(Person) :- child_of(_, Person), female(Person).

% Tests
% mother(diana).   => true
% mother(charles). => false
% mother(A).


%%% Exercise 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define rule `sibling(Person1, Person2)`
% that holds if Person1 is a sibling of Person2

% Rule
sibling(Person1, Person2) :- child_of(Parent, Person1), child_of(Parent, Person2), Person1 \= Person2.

% Tests
% sibling(charles, diana). => false
% sibling(charles, anne).  => true
% sibling(william, harry). => true
% sibling(A, B).


%%% Exercise 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Who is divorced in the House of Windsor?

% Rule
%divorced(Person) :- married(Person, Spouse1), married(Person, Spouse2), Spouse1 \= Spouse2.
%divorced(Person) :- married(Spouse1, Person), married(Spouse2, Person), Spouse1 \= Spouse2.
divorced(Person) :- ((married(Person, Spouse1), married(Person, Spouse2));
                     (married(Spouse1, Person), married(Spouse2, Person))), Spouse1 \= Spouse2.

% Tests
% divorced(anne).    => true
% divorced(diana).   => false
% divorced(andrew).  => false
% divorced(charles). => true
% divorced(A).


%%% Exercise 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define rule `ancestor_of(Descendant, Ancestor)`
% that holds if Ancestor is an ancestor of Descendant

% Rule
ancestor_of(Descendant, Ancestor) :- child_of(Ancestor, Descendant).
ancestor_of(Descendant, Ancestor) :- child_of(Person, Descendant), ancestor_of(Person, Ancestor).

% Tests
% ancestor_of(charles, philip).      => false.
% ancestor_of(philip, charles).      => true.
% ancestor_of(charles, harry).       => true.
% ancestor_of(philip, harry).        => true.
% ancestor_of(philip, sarah.         => false.
% ancestor_of(philip, elizabeth_II). => false.
% ancestor_of(A, B).

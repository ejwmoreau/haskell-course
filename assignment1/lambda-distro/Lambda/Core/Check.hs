{-# OPTIONS_GHC -Wall #-}

-- | Lambda Core Calculus type checker.
module Lambda.Core.Check where
import Lambda.Core.Exp


-- | Check an expression for problems.
check   :: [(Macro, Exp)]       -- ^ Table of macros.
        -> [(Var,   Type)]      -- ^ Types of free variables (the type environment)
        -> Exp                  -- ^ Expression to check.
        -> Either ErrorType Type

-- Checks for the types of various expressions.
check _macros _env _xx
 = case _xx of

    -- Checks primitives (t-prim).
    XPrim (PBool _)
     -> Right $ TPrim TBool
    XPrim (PNat _)
     -> Right $ TPrim TNat
    XPrim (PNot)
     -> Right $ TFun (TPrim TBool) (TPrim TBool)
    XPrim (PAnd)
     -> Right $ TFun (TPrim TBool) (TFun (TPrim TBool) (TPrim TBool))
    XPrim (POr)
     -> Right $ TFun (TPrim TBool) (TFun (TPrim TBool) (TPrim TBool))
    XPrim (PAdd)
     -> Right $ TFun (TPrim TNat) (TFun (TPrim TNat) (TPrim TNat))
    XPrim (PEquals)
     -> Right $ TFun (TPrim TNat) (TFun (TPrim TNat) (TPrim TBool))
    XPrim (PIf)
     -> Left $ ErrorTypeIfNotPrimitive

    -- Checks if (t-if).
    XApp (XApp (XApp (XPrim PIf) b1) (e1)) (e2)
     -> case check _macros _env b1 of
        Right (TPrim TBool) -> case check _macros _env e1 of
            Right (t1) -> case check _macros _env e2 of
                Right (t2) -> if (t1 == t2)
                                then Right (t2)
                                else Left (ErrorTypeTIfCase)
                _ -> Left (ErrorTypeInvalidType)
            _ -> Left (ErrorTypeInvalidType)
        _ -> Left (ErrorTypeInvalidType)

    -- Checks app (t-app).
    XApp e1 e2
     -> case check _macros _env e1 of
        Right (TFun t1 t2) -> case check _macros _env e2 of
            Right (t3) -> if (t1 == t3)
                            then Right (t2)
                            else Left (ErrorTypeTAppCase)
            _ -> check _macros _env e1
        Right (TPrim _) -> Left (ErrorTypeInvalidType)
        _ -> check _macros _env e1

    -- Checks abs (t-abs).
    XAbs v t e
     -> case t of
        Just t1 -> case check _macros _env (XVar v) of
            Right (t2) -> case check _macros _env e of
                Right (t3) -> if (t1 == t2)
                                then Right (TFun t1 t3)
                                else Left (ErrorTypeTAbsCase)
                _ -> Left (ErrorTypeInvalidType)
            _ -> Left (ErrorTypeInvalidType)
            where _env = [(v, t1)] ++ _env
        _ -> Left (ErrorTypeNothingCase)

    -- Checks var (t-var).
    XVar v
     -> case (lookup v _env) of
        Just x -> Right (x)
        _ -> Left (ErrorTypeNothingCase)

    -- Checks any other case that is unknown.
    _
     -> Left $ ErrorTypeUnknownExpression


-- | Problems we can detect when type checking expressions.
data ErrorType
        -- | Variable is not present in the environment.
        = ErrorTypeUnboundVar Var

        -- | Scrutinee of an if-expression is not a Bool.
        | ErrorTypeIfNotBool

        -- | Error for if not being a primitive.
        | ErrorTypeIfNotPrimitive

        -- | Error for unknown expressions.
        | ErrorTypeUnknownExpression

        -- | Error for t-if problems.
        | ErrorTypeTIfCase

        -- | Error for t-app problems.
        | ErrorTypeTAppCase

        -- | Error for t-abs problems.
        | ErrorTypeTAbsCase

        -- | Error for invalid types.
        | ErrorTypeInvalidType

        -- | Error for Nothing cases.
        | ErrorTypeNothingCase

        -- | Error for failing at sparsex in tests.
        | ErrorTypeFailTypeEvaluation

        deriving (Eq, Show)

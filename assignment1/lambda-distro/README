
-------------------------------------------------------------------------------
COMP3109 Assignment 1. Lambda the Ultimate.

Name

  Elie Moreau


What tasks did you complete?

  Task 1 and 2


Did you get stuck, or couldn't work something out?

  Task 1
    - I was very confused on what exactly was expected of us to implement.
    - I don't think enough Haskell was taught to us to know what to do.

  Task 2
    - This was also confusing on how to implement.
    - The description was vague on what exactly was expected of us.
    - There were some functions in Haskell that would have helped a lot if I
      knew they existed earlier.
    - It was difficult to figure out how to implement test cases that would
      check the types.


-------------------------------------------------------------------------------
1. Install the Haskell Platform.
   Current version is 7.10.2
   https://www.haskell.org/platform/

   If this works you should now have the 'ghc', 'ghci' and 'cabal' commands.

   Cabal is the Haskell package manager and build tool.

   You can check the current install ghc version:

   $ ghc --version


2. Update the cabal package directory.
   This gets the list of available libraries from the online Hackage database.
   https://hackage.haskell.org/

   $ cabal update


3. Install the 'lambda' package.

   $ cd lambda
   $ cabal install


4. The executable will be installed in your system, 
   though the exact place depends on your OS.

   On OSX:   /Users/USER/Library/Haskell/bin
   On Linux: ~/.cabal/bin

   Add the correct directory to your $PATH.

   The 'cabal' build tool copies the executable to one of the above 
   installation directories. However, it is also stored locally under
   dist/build/lambda/lambda


5. Run the interpreter.
   You need to do this from the same directory as the 'Prelude.macros' file
   so that it can find it.
  
   $ lambda

   > (\f. f foo) (\x. x)
   (\x. x) foo
   foo
   [2 steps]

   > #add #one #two
   (\m. \n. \s. \z. m s (n s z)) #one #two
   (\n. \s. \z. #one s (n s z)) #two
   \s. \z. #one s (#two s z)
   ...
   \s. \z. s (s (s ((\z. z) z)))
   \s. \z. s (s (s z))
   [23 steps]


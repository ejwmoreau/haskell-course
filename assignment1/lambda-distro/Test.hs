{-# OPTIONS_GHC -Wall #-}

module Main
        ( module Test.Framework
        , test)
where
import Test.Framework
import Lambda.Core.Exp
import Lambda.Core.Check


-- | Run all the tests, printing the results to the console.
--
--   You can either load this file in ghci and evaluate 'test',
--   or run it from the unix command-line with:
--
-- @
--   ghc Test.hs -e "test"
-- @
--
test :: IO Bool
test = fmap and $ mapM testOne tests


tests :: [Test]
tests =
 -- Type parser.
 [ Match "parsing Nat type"
        pparset Just
        "Nat"

 , Match "parsing function type"
        pparset Just
        "(Nat -> Bool) -> Nat"

 -- Expression parser.
 , Match "parsing expressions"
        pparsex Just
        "if (equals (add 2 2) 4) 10 (\\x : Nat. x)"

 -- Reduction -> not.
 , Same  "eval not 1"
        (peval "not true")
        (Right "false")

 , Same  "eval not 2"
        (peval "not (not true)")
        (Right "true")

 -- Reduction -> and.
 , Same  "eval and 1"
        (peval "and false true")
        (Right "false")

 , Same  "eval and 2"
        (peval "and true true")
        (Right "true")

 -- Reduction -> or.
 , Same  "eval or 1"
        (peval "or false true")
        (Right "true")

 , Same  "eval or 2"
        (peval "or false false")
        (Right "false")

 -- Reduction -> add.
 , Same  "eval add 1"
        (peval "add 2 3")
        (Right "5")

 , Same  "eval add 2"
        (peval "add 2 (add 4 5)")
        (Right "11")

 -- Reduction -> equals.
 , Same  "eval equals 1"
        (peval "equals 3 5")
        (Right "false")

 , Same  "eval equals 2"
        (peval "equals 6 6")
        (Right "true")

 -- Reduction -> if.
 , Same  "eval if 1"
        (peval "if true 4 5")
        (Right "4")

 , Same  "eval if 2"
        (peval "if false 5 (add 3 4)")
        (Right "7")

 -- Type Checking -> t-abs.
 , Same  "eval t-abs 1"
        (teval "(\\x:Nat. x) 1")
        (Right (TPrim TNat))

 , Same  "eval t-abs 2"
        (teval "(\\x:Bool. x) false")
        (Right (TPrim TBool))

 , Same  "eval t-abs 3"
        (teval "(\\x:Nat. x) false")
        (Left (ErrorTypeTAppCase))

 -- Type Checking -> t-var.
 , Same  "eval t-var 1"
        (teval "3")
        (Right (TPrim TNat))

 , Same  "eval t-var 2"
        (teval "fals")
        (Left (ErrorTypeNothingCase))

 , Same  "eval t-var 3"
        (teval "3f")
        (Left (ErrorTypeInvalidType))

 -- Type Checking -> t-if.
 , Same  "eval t-if 1"
        (teval "if true 3 4")
        (Right (TPrim TNat))

 , Same  "eval t-if 2"
        (teval "if true false true")
        (Right (TPrim TBool))

 , Same  "eval t-if 3"
        (teval "if true 3 true")
        (Left (ErrorTypeTIfCase))

 -- Type Checking -> t-app.
 , Same  "eval t-app 1"
        (teval "add 3 4")
        (Right (TPrim TNat))

 , Same  "eval t-app 2"
        (teval "and false 3")
        (Left (ErrorTypeTAppCase))

 , Same  "eval t-app 3"
        (teval "equals 3 true")
        (Left (ErrorTypeTAppCase))

 -- Type Checking -> t-prim.
 , Same  "eval t-prim 1"
        (teval "add")
        (Right (TFun (TPrim TNat) (TFun (TPrim TNat) (TPrim TNat))))

 , Same  "eval t-prim 2"
        (teval "no")
        (Left (ErrorTypeNothingCase))

 , Same  "eval t-prim 3"
        (teval "people")
        (Left (ErrorTypeNothingCase))

 -- Multiple checking.
 , Same  "eval complex 1"
        (teval "if (equals 5 5) (add 3 4) (if true 3 4)")
        (Right (TPrim TNat))

 , Same  "eval complex 2"
        (peval "if (equals 5 5) (and (not false) false) (equals (add 1 3) 4)")
        (Right "false")

 , Same  "eval complex 3"
        (peval "if (equals 5 5) (and (not false) false) (equals (add 1 3) true)")
        (Right "if true false (equals 4 true)")

 -- This expression is ill-typed and stuck.
 -- The evaluator cannot reduce it further.
 , Same  "eval bad 1"
        (peval "add 2 (add 3 not) false")
        (Right "add 2 (add 3 not) false")

 -- If-expressions don't work in the assignment distribution.
 , Same  "eval if"
        (peval "if (equals (add 2 3) 5) 10 20")
        (Right "10")
 ]

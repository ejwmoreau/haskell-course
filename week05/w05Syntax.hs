import Prelude
import Control.Monad

--------------------------------------------------------------------------------
-- | Monad Definition
--
--class Monad m where
--  return :: a   -> m a
--  (>>=)  :: m a -> (a -> m b) -> m b
--
--instance Monad Maybe where
--  return x      = Just x
--  Nothing >>= _ = Nothing
--  Just x >>= f  = f x
--
--instance Monad List where
--  return x      = [x]
--  xs >>= f      = concat (map f xs)

--------------------------------------------------------------------------------
-- | Exercise 4
--   Implement join using bind, Kleisli composition, and do-notation

-- Join with bind operator (>>=)
join1 :: Monad m => m (m a) -> m a
join1 a1 = a1 >>= (\a2 -> a2)

-- Join with Kleisli composition (>=>)
-- (>=>) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
join2 :: Monad m => m (m a) -> m a
join2 a1 = (return >=> (>>= (\a2 -> a2))) a1

-- Join with do-notation (do)
join3 :: Monad m => m (m a) -> m a
join3 a1 = do y <- return a1
              y >>= (\a2 -> a2)

--------------------------------------------------------------------------------
-- | Exercise 6
--   Implement the following:
--   sequence :: Monad m => [m a] -> m a

combine1 :: a -> [a] -> [[a]]
combine1 _ [] = []
combine1 e (x : xs) = [[e, x]] ++ (combine1 e xs)

combine2 :: [a] -> [a] -> [[a]]
combine2 [] _ = []
combine2 (x : xs) y = (combine1 x y) ++ (combine2 xs y)

--sequence1 :: Monad m => [m a] -> m a
sequence1 :: [[a]] -> [[a]]
sequence1 [] = []
sequence1 (x : []) = [x]
sequence1 (x : (y : xs))
-- = (\y1 -> combine1 y1 x2) ++ sequence1 xs2
 = do first <- combine2 x y
      sequence1 ([first] ++ xs)
-- = combine2 x y

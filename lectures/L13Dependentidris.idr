{-
   1) Install Idris from Hackage.
      It's written in Haskell.

      $ cabal update
      $ cabal install idris


   2) Idris syntax is similar to Haskell, but has some differences.

      Thing              Haskell              Idris
      ~~~~~              ~~~~~~~              ~~~~~

      Type Signatures    foo :: Int -> Int    foo : Int -> Int

      Lambdas            (\x -> x + 1)        (\x => x + 1)

      Case-expressions   case x of            case x of
                          True  -> 5           True  => 5
                          False -> 6           False => 6
-}

-------------------------------------------------------------------------------
module Main
%hide Prelude.Either
%hide Prelude.Either.Left
%hide Prelude.Either.Right
%hide Prelude.Nat.plus
%hide Refl


-- And and Or -----------------------------------------------------------------
data Or  a b    = Left a | Right b
data And a b    = Both  a b


-- Take a p and return a function that only returns p (ignores q)
total
ex1 : p -> (q -> p)
ex1 xp = \xq => xp


-- Takes a p and function converting p to q, then applies function to p
total
ex2 : And p (p -> q) -> q
ex2 xppq
 = case xppq of
        Both xp xpq => xpq xp


--Takes a p and q, then returns function taking r and returning q and r
total
ex3 : And p q -> (r -> (And q r))
ex3 xpq
 = case xpq of
        Both xp xq => \xr => Both xq xr


total
ex4 : ((Or p q) -> q) -> p -> And p q
ex4 f xp
 = Both xp (f (Left xp))


total
ex5 : Or p (And q r) -> Or p q
ex5 xx
 = case xx of
        Left  xp  => Left xp
        Right xqr => case xqr of
                        Both xq xr => Right xq


total
ex6 : Or (p -> q) r -> p -> Or r q
ex6 xpqr xp
 = case xpqr of
        Left  xpq       => Right (xpq xp)
        Right r         => Left  r


-- Negation -------------------------------------------------------------------
--
-- These definitions are in the Idris standard library.
--
-- A data type with no constructors.
-- We can never have a value of this type.
--
--  data Void
--
--
-- Encode negation of some proposition as a function that takes
-- a value of that type and produces a void. We can never have a value
-- of type Void, so 'Not a' means we can never have a value of type 'a'.
--
--  type Not a = a -> Void
--
--
-- A function that takes a Void value and produces anything we want.
-- This encodes the reasoning principle "ex falso quodlibet" or
-- "from falsehood, anything follows" / proof by contradiction.
--
--  void : Void -> b
--

-- If b, then return b, else if a, then convert a -> void -> b and return it
total
ex7 : Or a b -> Not a -> b
ex7 xx nopeA
 = case xx of
        Left  x => void (nopeA x)
        Right y => y


-- Same as above
total
ex10 : Or p q -> Not p -> q
ex10 xpq xnp
 = case xpq of
        Left  xp => void (xnp xp)
        Right xq => xq


total
ex11 : (p -> q) -> Not q -> Not p
ex11 f nq xp
 = nq (f xp)


-- Not possible in constructive logic.
--
-- If we treat "Not p" meaning as "we can never have a value of type 'p'",
-- then 'Not (Not p)' does not mean we have a 'p'.
--
--   nup12 : Not (Not p) -> p
--
--
-- Similarly, we cannot come up with a witness to this statement.
--
--   nup13 : Or p (Not p)
--
-- To see why, lets add the (implicit) quantifiers.
--
--   nup13 : (p : Type) -> Or p (Not p)
--
-- To define nup13 we would need to write a function that can produce a
-- a proof (or proof of negation) for ANY logical statement. Or to put it
-- another way: our function would need to produce a value of ANY type,
-- or a (Not p) function that works for ANY type.
--

-- Peano ---------------------------------------------------------------------
-- data Nat : Type where
--  Z : Nat               -- zero
--  S : Nat -> Nat        -- construct the successor of some Nat.
--

-- | Add two natural numbers.
plus : Nat -> Nat -> Nat
plus Z      n   = n
plus (S n1) n2  = S (plus n1 n2)


-- Dependency ---------------------------------------------------------------

-- | A vector is a list where the length of the list is encoded in its type.
data Vect : Nat -> Type -> Type where
  Nil   : Vect Z a
  (::)  : a -> Vect k a -> Vect (S k) a


foo : Vect 4 Nat
foo = [1, 2, 3, 4]


bar : Vect 6 Nat
bar = [1, 2, 3, 4, 5, 6]


-- | Append for vectors.
(++)    : Vect n a -> Vect m a -> Vect (plus n m) a
(++) Nil       ys = ys
(++) (x :: xs) ys = x :: xs ++ ys


-- | In the definition above, the quantifiers for 'a' 'n' and 'm' are
--   implicit. Here is a version of append which explicitly binds those values.

app     :  (a : Type) -> (n : Nat) -> (m : Nat)
        -> Vect n a -> Vect m a -> Vect (plus n m) a

app a Z     m Nil       ys = ys
app a (S k) m (x :: xs) ys = x :: app a k m xs ys



{- Typing rules

 * Here are the rules for our usual (non-dependent)
   function abstraction and application.

            Env, x : T1 |- e2 :: T2
    ---------------------------------------- (abs)
       Env |- (\x : T1. e2) :: T1 -> T2


    Env |- e1 :: T1 -> T2      Env |- e2 :: T1
  ------------------------------------------- (app)
             Env |- e1 e2 :: T2


 * With dependent abstraction, the binding variable 'x'
   also appears in the type. The type 'T2' can mention 'x'.

             Env, x : T1 |- e2 :: T2
   --------------------------------------------- (dep-abs)
    Env |- (\x : T1. e2) :: (x : T1) -> T2


 * When we perform a dependent application, we substitute
   the argument expression into the type of the body of
   the function.

    Env |- e1 :: (x : T1) -> T2   Env |- e2 :: T1
   --------------------------------------------- (dep-app)
              Env |- e1 e2 :: T2[e2/x]
-}



-- Proof --------------------------------------------------------------------

-- | Introduce a data type to encode the fact that two things are equal.
--   The only way we can construct a value of this type is via the
--   'Refl' constructor, that ensures the two things *are* really equal.
data Equals : a -> b -> Type where
 Refl : (x : a) -> Equals x x


-- Prove that equality respects the successor constructor.
total
eq_resp_S    :  (m : Nat) -> (n : Nat)
             -> Equals m n -> Equals (S m) (S n)
eq_resp_S x x (Refl x) = Refl (S x)


-- Zero plus some number is that number.
-- Idris will reduce 'plus Z n' to 'n' when type checking.
total
plusReduces  : (n : Nat) -> Equals (plus Z n) n
plusReduces n = Refl n


-- Some number equals that number plus zero.
-- In this proof we make use of the two above.
-- Proof composition is function application.
total
plusReducesZ : (n : Nat) -> Equals n (plus n Z)
plusReducesZ Z     = Refl Z
plusReducesZ (S k) = eq_resp_S k (plus k Z) (plusReducesZ k)



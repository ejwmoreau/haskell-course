
-- COMP3109 Programming Languages and Paradigms.
-- Week 4 tutorial.
--
-- Install the Haskell Platform.
--  https://www.haskell.org/platform/
--
-- In the console, load this file in ghci like:
--  $ ghci T3.hs
--
-- The Prelude module defines standard types and operators.
-- We will define our own versions of the following functions,
-- so we hide the versions imported from the Prelude.
import Prelude
 hiding ( Maybe (..), Monoid(..)
        , foldr, filter, insert, sum, length
        , first, last, lookup, elem, replicate)

-- Here are the types of useful functions from the Prelude:
--
--   fst  :: (a, b) -> a                 -- first component of a pair
--   snd  :: (a, b) -> b                 -- second component of a pair
--   (&&) :: Bool -> Bool -> Bool        -- and
--   (||) :: Bool -> Bool -> Bool        -- or
--   (++) :: [a] -> [a] -> [a]           -- list append
--   map  :: (a -> b) -> [a] -> [b]


-------------------------------------------------------------------------------
-- Q1. Replacement
--
-- The filter function selects the elements which match the given predicate.
--
-- Note that we're using the sugared form of lists, so
--   as a type,     [a]                means  List a
--   as a value,    [1, 2, 3]          means  Cons 1 (Cons 2 (Cons 3 Nil))
--   which is also  1 : (2 : (3 : []))
--
filter  :: (a -> Bool) -> [a] -> [a]
filter p [] = []
filter p (x : xs)
 = if p x then x : filter p xs
          else filter p xs


-- part a)
-- Write a function that replaces *just the first* value that matches the
-- given predicate with the provided element.
--
-- Eg: replace (\x -> x > 3) 0 [1, 2, 3, 4, 5]
--       = [1, 2, 3, 0, 5]
--
replace :: (a -> Bool) -> a -> [a] -> [a]
replace f e [] = []
replace f e (x : xs)
 = if f x then (e : xs) else (x : replace f e xs)


-------------------------------------------------------------------------------
-- Q2. Trees
--
-- Here is a type for binary trees.
--
data Tree a
        = Leaf
        | Node a (Tree a) (Tree a)
        deriving Show


-- part a)
-- Write a function that converts a Tree to a list of all its elements,
-- while preserving the left-to-right order of elements in the tree.
--
-- Eg: flatten (Node 3 Leaf (Node 4 Leaf (Node 5 Leaf Leaf)))
--        = [3, 4, 5]
--
flatten :: Tree a -> [a]
flatten Leaf = []
flatten (Node e l r)
 = flatten l ++ [e] ++ flatten r

-- part b)
-- Write a function to insert a new element into a sorted tree at
-- the correct position -- so that the sorted order is maintained.
--
-- Eg: flatten (insert 6 tree1)
--       = [1, 5, 6, 7, 8, 9]
--
tree1 = Node 5 (Node 1 Leaf Leaf)
               (Node 8 (Node 7 Leaf Leaf)
                       (Node 9 Leaf Leaf))

insert :: Ord a => a -> Tree a -> Tree a
insert e Leaf = (Node e Leaf Leaf)
insert e1 (Node e2 l r)
 = if e1 < e2
   then (Node e2 (insert e1 l) r)
   else (Node e2 l (insert e1 r))


--------------------------------------------------------------------------------
-- Q3. Folding (catamorphisms)
--
-- Here is the definition of foldr from the lectures,
-- using the standard Haskell sugar for lists.
--
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z []       = z
foldr f z (x : xs) = f x (foldr f z xs)

-- And some examples
sum :: Num a => [a] -> a
--sum    = foldr (+) 0                      -- Old
sum ls = foldr (+) 0 ls                     -- New

length :: [a] -> Int
--length = foldr (\x y -> 1 + y) 0          -- Old
length ls = foldr (\x y -> 1 + y) 0 ls      -- New

-- part a)
-- Define a function in terms of foldr that checks if a given value
-- is an element of some list.
elem :: Eq a => a -> [a] -> Bool
elem e ls = foldr (\x y -> y || (x == e)) False ls


-------------------------------------------------------------------------------
-- Q5. Monoids

-- Here is a datatype that represents whether we have a value or not.
data Maybe a
        = Nothing
        | Just a
        deriving Show


-- The following monoid might be handy...
class Monoid a where
 mzero   :: a
 mappend :: a -> a -> a

instance Monoid (Maybe a) where
 mzero   = Nothing

 mappend (Just x) (Just y) = Just x
 mappend (Just x) Nothing  = Just x
 mappend Nothing  (Just y) = Just y
 mappend _        _        = Nothing


-- part a)
-- Define a function in terms of foldr that takes a key, a list of key-value
-- pairs, and returns Just a value if there is a value associated with the
-- corresponding key, or Nothing otherwise.
--
lookup :: Eq k => k -> [(k, v)] -> Maybe v
lookup k ls = foldr (\x y -> mappend y (if (k == fst x)
                                          then (Just (snd x))
                                          else Nothing)) Nothing ls


-------------------------------------------------------------------------------
-- Q6. Unfolding (anamorphisms)
--
-- Unfold takes a creation function, and a initital state, and produces a list.
--
--  If the creation function wants to make an element then it
--  should return  Just  the element (a), and a new state (s),
--  otherwise it should return Nothing.
--
unfold :: (s -> Maybe (a, s)) -> s -> [a]
unfold f s
 = case f s of
        Nothing         -> []
        Just (x, s')    -> x : unfold f s'


-- part a)
-- Define a function in terms of unfold that replicates a value
-- the given number of times.
replicate :: Int -> a -> [a]
replicate n e = unfold (\x -> if x == 0 then Nothing else Just (e, x-1)) n


-- part b)
-- Define a function in terms of unfold that produces a range of values,
-- Eg:  range 1 5  => [1, 2, 3, 4, 5]
range :: Int -> Int -> [Int]
range n m = unfold (\x -> if x > m then Nothing else Just (x, x+1)) n


-- part c)
-- Here is the factorial function from lectures, expressed recursively.
fac :: Int -> Int
fac 0 = 1
fac n = n * fac (n - 1)

-- Express factorial as a hylomorphism, meaning a combination
-- of a fold and an unfold.
hfac :: Int -> Int
hfac n = foldr (\x y -> x * y) 1 (range 1 n)


-------------------------------------------------------------------------------
-- Q7. MapReduce
--
-- Here is a simple sequential implementation of the mapReduce pattern
-- of computation. It doesn't run in parallel on a cluster of machines,
-- but it does give the correct answer. It is slightly more general than
-- the one present in the lecture, as the input and output keys can
-- have different types.
mapReduce
        :: Ord k2
        => (k1 -> [(k2, a)])     -- The mapper function.
        -> (a -> a -> a)         -- The reducer function.
        -> [k1]                  -- Input list
        -> [(k2, a)]             -- Output result.

mapReduce f g xs
 = let  ms      = concat  (map f xs)
        ss      = shuffle ms
   in   map (\(k, ys) -> (k, reduce g ys)) ss


-- The shuffle phase sorts collects together all values that have
-- the same key.
shuffle :: Eq k => [(k, v)] -> [(k, [v])]
shuffle [] = []
shuffle ((k1, v1) : rest)
 = let  match   = map snd (filter (\(k, v) -> k == k1) rest)
        nope    = filter (\(k, v) -> k /= k1) rest
   in   (k1, (v1 : match)) : shuffle nope


-- The reduce phase uses a fold operation that requires at least one element
-- to be associated with each key. It uses this element as the starting value
-- in the fold.
reduce :: (a -> a -> a) -> [a] -> a
reduce f []       = error "reduce: no elements"
reduce f (x : xs) = foldr f x xs


-- part a)
-- Define the word-count example from the lectures using the above
-- mapReduce function.
wordcount :: [String] -> [(String, Int)]
wordcount ls = mapReduce (\x -> [(x, 1)]) (+) ls


-- part b)
-- Define a function to take the length of the input using mapReduce.
-- (hint) You might need to do some simple post-processing of the result
--        of the actual mapReduce function.
mlength :: [a] -> Int
mlength ls = let result = mapReduce (\_ -> [(0,1)]) (+) ls
             in  snd(head(result))


-- part c)
-- Define the elem function from Q4 using mapReduce.
melem :: (Num a, Ord a) => a -> [a] -> Bool
melem n ls = let result = mapReduce (\x -> if x == n then [(1,True)] else []) (||) ls
             in  if length result == 0 then False else True


-- part d) (optional)
-- Define the lookup function from Q5 using mapReduce.
mlookup :: Ord k => k -> [(k, v)] -> Maybe v
mlookup k ls = Just (snd (head (mapReduce
                                (\x -> if (fst(x) == k)
                                        then [(1, snd(x))]
                                        else [])
                                (\x y -> x) ls )))

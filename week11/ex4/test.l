digit [0-9]
letter [a-zA-Z]
%%
"begin" {puts("keyword: begin");}
"end" {puts("keyword: end");}
{digit}+ {printf("integer literal: %s\n",yytext);}
{letter}({letter}|{digit})* {printf("identifier: %s\n",yytext);}
[ \t\n\r]

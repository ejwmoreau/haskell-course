#include <stdio.h>

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Needs one argument\n");
        return 1;
    }

    int num = 0;
    sscanf(argv[1], "%d", &num);

    if (num < 1) {
        printf("Needs to be greater than 0\n");
        return 1;
    }

    int count = 1;
    for (int i = 1; i <= num; i++) {
        count = count * i;
    }

    printf("Factorial: %d\n", count);
    return 0;
}

import Prelude

data Season = Spring | Summer | Autumn | Winter
            deriving(Show, Eq)

---------- Redo of Quiz ----------
stutter1 :: [a] -> [a]
stutter1 [] = []
stutter1 (x : xs) = [x] ++ [x] ++ (stutter1 xs)

stutters1 :: [Int] -> [a] -> [a]
stutters1 [] _ = []
stutters1 _ [] = []
stutters1 (n : ns) (x : xs)
 = if (n == 0)
    then (stutters1 ns xs)
    else if (n == 1)
          then [x] ++ (stutters1 ns xs)
          else (stutter1 [x]) ++ (stutters1 ((n-2) : ns) (x : xs))

coldness1 :: [Season] -> Bool
coldness1 [] = False
coldness1 ls = foldr (\x y -> ((Winter == x) || y)) False ls


---------- Original Quiz ----------
stutter2 :: [a] -> [a]
stutter2 [] = []
stutter2 (x : xs) = [x] ++ [x] ++ (stutter2 xs)

stutters2 :: [Int] -> [a] -> [a]
stutters2 [] [] = []
stutters2 (n : ns) (x : xs)
 = if (n == 0)
    then (stutters2 ns xs)
    else [x] ++ (stutters2 ((n-1) : ns) (x : xs))

coldness2 :: [Season] -> Bool
coldness2 [] = False
coldness2 ls = foldr (\x y -> y || (Winter == x)) False ls

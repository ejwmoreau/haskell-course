-- Comp3109 Week 12 Tutorial.
--  You can either check these in Idris, or just do them by hand.


-- Exercise 1 -----------------------------------------------------------------
-- Predicate Calculus.
-- If you're not sure how to do these, then check out the examples
-- in the lecture notes. Cover up the definitions and try to re-prove them.

data And a b = Both a b
data Or  a b = Left a | Right b

-- These are defined in the base library.
-- type Not a = a -> Void
-- void : Void -> b

-- Here is a simple one as a warmup.
total
ex1 : (p -> q -> r) -> And p q -> r
ex1 fpqr (Both p' q') = fpqr p' q'

-- To prove theorems involving negation it can be helpful to first
-- expand the definition of Not, so that you can see what is going on.
total
ex2 : And a (Not b) -> Not (a -> b)
ex2 (Both a' fnb) = \fab => fnb (fab a')
-- a' : a
-- fnb : b -> Void
-- fab : a -> b
-- WE WANT result : (a -> b) -> Void

-- In constructive logic the theorems (Or a (Not a)) and (Not (Not a) -> a)
-- are not provable. However, we can prove that if we had one, then we
-- could prove the other.
--

-- Or a (a -> void) -> (((a -> void) -> void) -> a)
total
ex3 : Or a (Not a) -> (Not (Not a) -> a)
ex3 (Left a') fnna = a'
ex3 (Right fna) fnna = void (fnna fna)

--Alternative:
--ex3 (Left a') = \_ => a'
--ex3 (Right fna) = \fnna => void (fnna fna)


-- Exercise 2 -----------------------------------------------------------------
-- The Calculus of Chinchillas.

-- We can use Idris to reason about things in the world,
-- by encoding external concepts as abstract data types and axioms.

-- The abstract type of Chinchillas (cute rodents)
data Chinchilla

-- Values of these types are witnesses to the given property.
data Cute  : Chinchilla -> Type
data Small : Chinchilla -> Type
data Young : Chinchilla -> Type
data Old   : Chinchilla -> Type
data Fuzzy : Chinchilla -> Type


-- We can encode external facts as axioms.
--   These functions have the given types, but if we were try to use them
--   at runtime they will just throw an exception. That's ok, because we can't
--   really pass a Chinchilla to a function. The Idris operational semantics
--   has no rules to reduce a Chinchilla... they are rodent's after all.
--
smallFuzzyCute : (x : Chinchilla) -> Or (Small x) (Fuzzy x) -> Cute x
smallFuzzyCute = believe_me "axiom"

youngOrOld     : (x : Chinchilla) -> Or (Young x) (Old x)
youngOrOld     = believe_me "axiom"

youngIsSmall   : (x : Chinchilla) -> Young x -> Small x
youngIsSmall   = believe_me "axiom"

oldIsFuzzy     : (x : Chinchilla) -> Old x   -> Fuzzy x
oldIsFuzzy     = believe_me "axiom"


-- The fact that all Chinchillas are cute is a universal Truth.
--   Construct a witness to this fact by combining the above functions.
total
allCute : (x : Chinchilla) -> Cute x
--allCute = believe_me "replace this by your definition"
allCute x' = smallFuzzyCute x' result
             where result = case (youngOrOld x') of
                                 (Left  r) => (Left  (youngIsSmall x' r))
                                 (Right r) => (Right (oldIsFuzzy   x' r))
--allCute x' = case (youngOrOld x') of
--                  (Left  r) => smallFuzzyCute x' (Left  (youngIsSmall x' r))
--                  (Right r) => smallFuzzyCute x' (Right (oldIsFuzzy   x' r))
